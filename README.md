# Pezesha k8s Loan Management Platform (Interview)

This repository contains a distributed microservices implementation using Kubernetes for a mock application that allows authenticated users to manage users (borrowers & lenders), deposits, withdrawals and issue mock loans via a mock MPESA service. 

## Project components

[![Project Components](https://i.imgur.com/2KWVR24.png)]()

The following are the main components and docker containers used.

The `pezesha_cmd` module runs on it's own docker container and is the authenticated entry point into the application that based on the kubernetes `deployment.yml` and uses the docker container [reayn3/pezesha_cmd](https://hub.docker.com/repository/docker/reayn3/pezesha_cmd). This is a java8 command line application that allows users to authenticate and select a set of options to manage loans. All requests made from this application are issued to the `pezesha_loan_management` running on itw own docker container via a REST API. The only fully tested option in this module is option 4 (Issue Loan).

[![Sample run for pezesha_cmd](https://i.imgur.com/tI14e5o.png)]()

The screenshot below shows the `pezesha_loan_management` application receiving the REST request from the `pezesha_cmd` container and recording the loan issuance on the mongo database. The mongo database runs on its own docker container [reayn3/pezesha_mongo](https://hub.docker.com/repository/docker/reayn3/pezesha_mongo). The document schema is defined below. The `pezesha_loan_management` application is written in Spring Boot.

[![Received request from pezesha_cmd to pezesha_loan_management](https://i.imgur.com/hDhYNz3.png)]()

The `pezesha_loan_management` application then sends a success message to the `pezesha_cmd` container application via REST confirming the successful entry of the loan issuance to the client. Once this is result is received by the `pezesha_cmd` application, it then posts a Producer message to the kafka+zookeeper instance running on its own container. This docker container is available at [reayn3/pezesha_kafka](https://hub.docker.com/repository/docker/reayn3/pezesha_kafka). 

The `pezesha_mpesa_service` is subscribed to the kafka container as a consumer to the topic `mpesa`. It therefore receives the message and records this to console to mimic sending out the loan or a loan status via MPESA. The `pezesha_mpesa_service` runs on its own docker container and is available at [reayn3/pezesha_mpesa_service](https://hub.docker.com/repository/docker/reayn3/pezesha_mpesa_service). The service is written in `nodejs`.

[![Received request from pezesha_cmd and published to Kafka](https://i.imgur.com/AtlFlMS.png)]()

### Docker containers used

1. pezesha_cmd: ([reayn3/pezesha_cmd](https://hub.docker.com/repository/docker/reayn3/pezesha_cmd))
2. pezesha_loan_management: ([reayn3/pezesha_loan_management](https://hub.docker.com/repository/docker/reayn3/pezesha_loan_management))
3. pezesha_mpesa_service: ([reayn3/pezesha_mpesa_service](https://hub.docker.com/repository/docker/reayn3/pezesha_mpesa_service))
4. pezesha_mongo: ([reayn3/pezesha_mongo](https://hub.docker.com/repository/docker/reayn3/pezesha_mongo))
5. pezesha_kafka: ([reayn3/pezesha_kafka](https://hub.docker.com/repository/docker/reayn3/pezesha_kafka))

Each of these containers runs on a single kubernetes `pod`.

### Programming technologies and frameworks used

1. pezesha_cmd: Java8, gradle, gson, apache-kafka, okhttp3
2. pezesha_loan_management: SpringBoot, spring-boot-starter-data-mongodb
3. pezesha_mpesa_service: nodejs, composer, kafka-node
4. pezesha_mongo: mongodb
5. pezesha_kafka: kafka, zookeeper

### Document Schema

Mongodb was selected and used in this project. The database schema developed against which Java 8 models were developed for the Spring Boot classes in `pezesha_loan_management` and the Java classes in `pezesha_cmd` is as shown below.


[![pezesha_mongo (Mongo DB) schema](https://i.imgur.com/DEhY4QO.png)]()


## Getting Started

This application was tested on a machine with the following characteristics:

```
reaganmbitiru@reaganmbitiru-MACH-WX9:~/Projects/interviews/pezesha$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 19.10
Release:	19.10
Codename:	eoan
```

In addition, `minikube` (if this application is being installed locally) as well as `kubectl` will be required to run this project. The following are the set of commands required to run the application. 

```
reaganmbitiru@reaganmbitiru-MACH-WX9:~/Projects/interviews/pezesha$: git clone https://reayn@bitbucket.org/reayn/pezesha.git && cd pezesha
reaganmbitiru@reaganmbitiru-MACH-WX9:~/Projects/interviews/pezesha$: minikube start
reaganmbitiru@reaganmbitiru-MACH-WX9:~/Projects/interviews/pezesha$ kubectl create -f deployment.yml

```

Running `kubectl dashboard` should monitor and display the progress of deployments.

[![Application Deployment Screenshot](https://i.imgur.com/Kdh6p8S.png)]()


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Reagan Mbitiru** - *Initial work* - [Reagan Mbitiru](https://github.com/Reagan)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details. Learn and enjoy!	


