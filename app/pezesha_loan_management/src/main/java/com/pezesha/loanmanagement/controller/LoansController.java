package com.pezesha.loanmanagement.controller;

import com.pezesha.loanmanagement.dao.LoanRepository;
import com.pezesha.loanmanagement.model.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LoansController {
    @Autowired
    private LoanRepository loanRepository;

    @GetMapping(value = "/loans", produces = "application/json")
    public List<Loan> getLoans() {
        return loanRepository.findAll();
    }

    @GetMapping(value = "/loans/{loanId}", produces = "application/json")
    public Loan getLoan(@PathVariable Integer loanId) throws Exception {
        Loan loan = loanRepository.getLoanWithId(loanId);
        if (loan != null)
            return loan;
        throw new Exception(String.format("Loan id %d not found.", loanId));
    }

    @GetMapping(value = "/loans/application_id/{applicationId}", produces = "application/json")
    public Loan getLoanForApplication(@PathVariable Integer applicationId) {
        return loanRepository.getLoanForApplicationId(applicationId);
    }

    @PostMapping(value = "/loans", consumes = "application/json", produces = "text/plain")
    public String addLoan(@RequestBody Loan loan) {
        Loan savedLoan = loanRepository.save(loan);
        return String.format("[Loan Management] 200 OK Saved new loan _Id: %s, id: %d", savedLoan._id, savedLoan.getLoanId());
    }


}
