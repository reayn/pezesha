package com.pezesha.loanmanagement.dao;

import com.pezesha.loanmanagement.model.LoanApplication;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanApplicationRepository extends MongoRepository<LoanApplication, String> {

    @Query(value = "{'borrower_loan_applications.user_id': ?0}")
    List<LoanApplication> getLoanApplicationsForUser(Integer userId);
}
