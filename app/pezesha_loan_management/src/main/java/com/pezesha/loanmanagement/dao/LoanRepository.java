package com.pezesha.loanmanagement.dao;

import com.pezesha.loanmanagement.model.Loan;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepository extends MongoRepository<Loan, String> {

    @Query(value = "{'loan_application_id': ?0}")
    Loan getLoanForApplicationId(Integer loanId);

    @Query(value = "{'loan_id': ?0}")
    Loan getLoanWithId(Integer loanId);
}
