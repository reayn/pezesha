package com.pezesha.loanmanagement.dao;

import com.pezesha.loanmanagement.model.Withdrawal;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WithdrawalRepository extends MongoRepository<Withdrawal, String> {

    @Query(value = "{'lender_withdrawals.user_id': ?0}")
    List<Withdrawal> getWithdrawalsForUser(Integer userId);
}
