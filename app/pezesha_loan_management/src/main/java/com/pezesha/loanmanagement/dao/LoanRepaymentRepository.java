package com.pezesha.loanmanagement.dao;

import com.pezesha.loanmanagement.model.LoanRepayment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoanRepaymentRepository extends MongoRepository<LoanRepayment, String> {

    @Query(value = "{'borrower_loan_repayments.loan_id': ?0}")
    List<LoanRepayment> getLoanRepaymentsForLoan(Integer loanId);
}
