package com.pezesha.loanmanagement.controller;

import com.pezesha.loanmanagement.dao.LoanApplicationRepository;
import com.pezesha.loanmanagement.model.LoanApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/loan_applications")
public class LoanApplicationsController {

    @Autowired
    private LoanApplicationRepository loanApplicationRepository;

    public List<LoanApplication> getLoanApplications() {
        return loanApplicationRepository.findAll();
    }

    public LoanApplication getLoanApplication(Integer loanApplicationId) throws Exception {
        Optional<LoanApplication> loanApplicationOptional = loanApplicationRepository.findById(String.valueOf(loanApplicationId));
        if (loanApplicationOptional.isPresent())
            return loanApplicationOptional.get();
        throw new Exception(String.format("Loan application id %d not found", loanApplicationId));
    }

    public List<LoanApplication> getLoanApplicationsForUser(Integer userId) {
        return loanApplicationRepository.getLoanApplicationsForUser(userId);
    }

}
