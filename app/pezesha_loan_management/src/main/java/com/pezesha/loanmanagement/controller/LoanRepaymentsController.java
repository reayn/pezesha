package com.pezesha.loanmanagement.controller;

import com.pezesha.loanmanagement.dao.LoanRepaymentRepository;
import com.pezesha.loanmanagement.model.LoanRepayment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/loan_repayments")
public class LoanRepaymentsController {

    @Autowired
    private LoanRepaymentRepository loanRepaymentRepository;

    public List<LoanRepayment> getLoanRepayments() {
        return loanRepaymentRepository.findAll();
    }

    public LoanRepayment getLoanRepayment(Integer loanRepaymentId) throws Exception{
        Optional<LoanRepayment> loanRepaymentOptional = loanRepaymentRepository.findById(String.valueOf(loanRepaymentId));
        if (loanRepaymentOptional.isPresent())
            return loanRepaymentOptional.get();
        throw new Exception(String.format("Loan repayment id %d not found", loanRepaymentId));
    }

    public List<LoanRepayment> getLoanRepaymentsForLoan(Integer loanId) {
        return loanRepaymentRepository.getLoanRepaymentsForLoan(loanId);
    }


}
