package com.pezesha.loanmanagement.controller;

import com.pezesha.loanmanagement.dao.WithdrawalRepository;
import com.pezesha.loanmanagement.model.Withdrawal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/withdrawals")
public class WithdrawalsController {

    @Autowired
    private WithdrawalRepository withdrawalRepository;

    @GetMapping(path = "/", produces = "application/json")
    public List<Withdrawal> getWithdrawals() {
        return withdrawalRepository.findAll();
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public Withdrawal getWithdrawal(Integer withdrawalId) throws Exception {
        Optional<Withdrawal> withdrawalOptional = withdrawalRepository.findById(String.valueOf(withdrawalId));
        if (withdrawalOptional.isPresent())
            return withdrawalOptional.get();
        throw new Exception(String.format("Withdrawal id %f not found", withdrawalId));
    }

    @GetMapping(path = "/users/{id}", produces = "application/json")
    public List<Withdrawal> getWithdrawalsForUser(Integer userId) {
        return withdrawalRepository.getWithdrawalsForUser(userId);
    }
}
