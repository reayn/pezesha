package com.pezesha.loanmanagement.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("users")
public class User {

    @Id
    private Integer id;
    private String firstName;
    private String lastName;
    private Date registrationDate;
    private Boolean activated;
    private Type type;

    public enum Type {
        LENDER, BORROWER
    }

    public User() {}

    public User(Integer id, String firstName, String lastName,
                Date registrationDate, Boolean activated, Type type) {
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setRegistrationDate(registrationDate);
        this.setActivated(activated);
        this.setType(type);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
