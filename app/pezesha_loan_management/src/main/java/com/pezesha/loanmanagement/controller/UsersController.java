package com.pezesha.loanmanagement.controller;

import com.pezesha.loanmanagement.dao.UserRepository;
import com.pezesha.loanmanagement.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "/users", produces = "application/json")
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public User getUser(Integer userId) throws Exception {
        Optional<User> userOptional = userRepository.findById(String.valueOf(userId));
        if (userOptional.isPresent())
            return userOptional.get();
        throw new Exception(String.format("User id %f not found", userId));
    }

    @PostMapping(path = "/users", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addUser(
            @RequestBody User user
    ) throws Exception {
        User createdUser = userRepository.save(user);
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/{id}")
                .buildAndExpand(createdUser.getId())
                .toUri();
        return ResponseEntity.created(location).build();
    }


}
