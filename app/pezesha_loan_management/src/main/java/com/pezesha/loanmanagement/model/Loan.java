package com.pezesha.loanmanagement.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Document(collection = "borrower_loans")
public class Loan {

    public enum Status {
        PAID, PENDING
    }

    @Id
    public ObjectId _id;

    @Field(value = "loan_id")
    private Integer loanId;

    @Field(value = "loan_application_id")
    private Integer loanApplicationId;

    @Field(value = "deposit_id")
    private Integer depositId;

    @Field(value = "disbursement_date")
    private Date disbursementDate;

    @Field(value = "status")
    private Status status;

    public Loan() {}

    public Loan(Integer loanId, Integer loanApplicationId,
                Integer depositId, Date disbursementDate,
                Status status) {
        this.setLoanId(loanId);
        this.setLoanApplicationId(loanApplicationId);
        this.setDepositId(depositId);
        this.setDisbursementDate(disbursementDate);
        this.setStatus(status);
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getLoanApplicationId() {
        return loanApplicationId;
    }

    public void setLoanApplicationId(Integer loanApplicationId) {
        this.loanApplicationId = loanApplicationId;
    }

    public Integer getDepositId() {
        return depositId;
    }

    public void setDepositId(Integer depositId) {
        this.depositId = depositId;
    }

    public Date getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(Date disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
