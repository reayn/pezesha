package com.pezesha.loanmanagement.dao;

import com.pezesha.loanmanagement.model.Deposit;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepositRepository extends MongoRepository<Deposit, String> {

    @Query(value = "{'lender_deposits.user_id': ?0}")
    List<Deposit> getDepositsForUser(Integer userId);

}
