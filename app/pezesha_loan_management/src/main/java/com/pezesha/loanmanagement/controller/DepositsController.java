package com.pezesha.loanmanagement.controller;

import com.pezesha.loanmanagement.dao.DepositRepository;
import com.pezesha.loanmanagement.model.Deposit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/deposits")
public class DepositsController {

    @Autowired
    private DepositRepository depositRepository;

    @GetMapping(path = "/", produces = "application/json")
    public List<Deposit> getDeposits() {
        return depositRepository.findAll();
    }

    @GetMapping(path = "/{id}", produces = "application/json")
    public Deposit getDeposit(Integer id) throws Exception{
        Optional<Deposit> depositOptional =  depositRepository.findById(String.valueOf(id));
        if (depositOptional.isPresent())
            return depositOptional.get();
        else throw new Exception(String.format("Deposit id %f not found", id));
    }

    @GetMapping(path = "/users/{id}", produces = "application/json")
    private List<Deposit> getDepositsForUser(Integer userId) {
        return depositRepository.getDepositsForUser(userId);
    }

}
