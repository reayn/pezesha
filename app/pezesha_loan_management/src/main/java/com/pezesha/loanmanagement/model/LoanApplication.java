package com.pezesha.loanmanagement.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("borrower_loan_applications")
public class LoanApplication {

    @Id
    private Integer loanApplicationId;
    private Integer userId;
    private Date loanApplicationDate;
    private Double amount;
    private Status status;

    public Integer getLoanApplicationId() {
        return loanApplicationId;
    }

    public void setLoanApplication(Integer loanApplicationId) {
        this.loanApplicationId = loanApplicationId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getLoanApplicationDate() {
        return loanApplicationDate;
    }

    public void setLoanApplicationDate(Date loanApplicationDate) {
        this.loanApplicationDate = loanApplicationDate;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public enum Status {
        PENDING, SUCCESSFUL, REJECTED
    }

}
