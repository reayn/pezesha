const kafka = require('kafka-node');
const express = require('express');

const kafkaHost = 'localhost:9092';
const app = express();

try {
    const Consumer = kafka.Consumer;
    const client = new kafka.KafkaClient(kafkaHost);
    let consumer = new Consumer(
        client,
        [{ topic: 'mpesa', partition: 0 }],
        {
            autoCommit: true,
            fetchMaxWaitMs: 1000,
            fetchMaxBytes: 1024 * 1024,
            encoding: 'utf8',
            fromOffset: false
        }
    );

    consumer.on('message', async function(message) {
        console.log(message);
    });
    consumer.on('error', function(err) {
        console.log('error', err);
    });
} catch(e) {
    console.log(e);
}

app.listen(8080,() => {
    console.log("Server is listening to port 8080");
});