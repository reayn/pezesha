package com.pezesha;

import com.pezesha.actions.BaseAction;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Properties;
import java.util.Scanner;

public class Pezesha<T extends BaseAction> {

    private Scanner scanner;
    private String baseUrl;
    private Integer port;

    public Pezesha() {
        scanner = new Scanner(System.in);
    }

    public static void main(String[] args) {
        try {
            Pezesha<BaseAction> pezesha = new Pezesha<>();
            pezesha.displayStartUpMessage();
            pezesha.loadConfig();
            if (pezesha.authenticate()) {
                pezesha.runLoanManagementService();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void displayStartUpMessage() {
        System.out.println("Pezesha Mock Loan Management Application \n" +
                "Copyright (C) 2020, Pezesha, Nairobi, Kenya. All Rights Reserved.\n" +
                "For issues, please contact support@pezesha.software\n");
    }

    private boolean authenticate() {
        String username, password = "";
        while (true) {
            System.out.println("Please enter login credentials");
            System.out.print("Username: ");
            username = scanner.next();
            System.out.print("Password: ");
            password = scanner.next();

            if (username.equals("admin") && password.equals("pass")) {
                break;
            }
            System.out.println("Invalid credentials!");
        }
        return true;
    }

    private void runLoanManagementService()
            throws IOException, ClassNotFoundException, IllegalAccessException,
            InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class<T>[] actions = loadSupportedActions();
        HashMap<Integer, Object> instantiatedActions = new HashMap<>();

        while (true) {
            System.out.println("\nPlease select action: (1, 2, 3...)");
            for (Class<T> action : actions) {
                if (!action.isInterface() && !Modifier.isAbstract(action.getModifiers())) {
                    Object t = action.getConstructor(String.class, Integer.class).newInstance(baseUrl, port);
                    Method getActionIdMethod = t.getClass().getMethod("getActionId");
                    Method getActionStringMethod = t.getClass().getMethod("getActionString");
                    Integer id = (Integer) getActionIdMethod.invoke(t);
                    String actionString = (String) getActionStringMethod.invoke(t);
                    System.out.println(id + ". " + actionString);
                    instantiatedActions.put(id, t);
                }
            }

            Integer option = scanner.nextInt();
            if (instantiatedActions.containsKey(option))
                System.out.println(runAction(instantiatedActions.get(option)));
            else
                System.out.println("Invalid option");
        }
    }

    private void loadConfig()
            throws IOException {
        final String CONFIG_FILE_PATH = "pezesha.conf";
        Properties properties = new Properties();
        InputStream inputStream = new FileInputStream(CONFIG_FILE_PATH);
        properties.load(inputStream);
        baseUrl = properties.getProperty("base-url");
        port = Integer.parseInt(properties.getProperty("port"));
    }

    private Class<T>[] loadSupportedActions()
            throws IOException, ClassNotFoundException {
        return (Class<T>[]) Utils.getClasses("com.pezesha.actions");
    }

    private String runAction(Object action)
        throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException {
        Method performActionMethod = action.getClass().getMethod("performAction");
        return (String) performActionMethod.invoke(action);
    }
}
