package com.pezesha.kafka;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class Producer {

    private static final String TOPIC = "mpesa";
    private final static String BOOTSTRAP_SERVERS = "localhost:9092";

    private static KafkaProducer<String, String> createProducer() {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "mpesa");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        return new KafkaProducer(props);
    }

    public void sendMessage(String message) {
        KafkaProducer<String, String> producer = createProducer();
        try {
            producer.send(new ProducerRecord<>(TOPIC, message, message));
        } catch (Exception e) {
            throw e;
        } finally {
            producer.flush();
            producer.close();
        }
    }
}
