package com.pezesha.actions;

import com.pezesha.kafka.Producer;
import com.pezesha.model.Loan;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

public class IssueLoanAction extends PostAction {

    private Producer producer;
    private Scanner scanner;
    private final String PATH = "/loans";

    public IssueLoanAction(String baseUrl, Integer port) {
        super(baseUrl, port);
        this.producer = new Producer();
        this.scanner = new Scanner(System.in);
        setPath(PATH);
    }

    public String getActionString() {
        return "Issue Loan";
    }

    public Integer getActionId() {
        return 4;
    }

    @Override
    public String performAction()
            throws IOException {
        Integer inputLoanId = Integer.parseInt((String) getInput("Enter new loan id: "));
        Integer inputLoanApplicationId = Integer.parseInt((String) getInput("Enter loan application id: "));
        Integer inputLoanDepositId = Integer.parseInt((String) getInput("Enter deposit id: "));

        Loan loan = new Loan();
        loan.setLoanId(inputLoanId);
        loan.setDepositId(inputLoanDepositId);
        loan.setDisbursementDate(new Date());
        loan.setLoanApplicationId(inputLoanApplicationId);
        loan.setStatus(Loan.Status.PAID);

        setBody(loan);

        String result = super.performAction();
        if (result.contains("200 OK"))
            producer.sendMessage(result);
        return result;
    }

    private Object getInput(String message) {
        System.out.print(message);
        return scanner.next();
    }
}
