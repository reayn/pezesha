package com.pezesha.actions;

public abstract class GetAction extends BaseAction {

    public GetAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }
}
