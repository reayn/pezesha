package com.pezesha.actions;

import java.io.IOException;

public interface Action {
    String getActionString();
    Integer getActionId();
    String performAction() throws IOException;
}
