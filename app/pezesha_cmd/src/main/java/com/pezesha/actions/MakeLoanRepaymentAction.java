package com.pezesha.actions;

public class MakeLoanRepaymentAction extends PostAction {

    public MakeLoanRepaymentAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }

    public String getActionString() {
        return "Repay Loan";
    }

    public Integer getActionId() {
        return 5;
    }

}
