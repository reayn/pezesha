package com.pezesha.actions;

public class DepositMoneyAction extends PostAction {

    public DepositMoneyAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }

    public String getActionString() {
        return "Deposit Money";
    }

    public Integer getActionId() {
        return 3;
    }
}
