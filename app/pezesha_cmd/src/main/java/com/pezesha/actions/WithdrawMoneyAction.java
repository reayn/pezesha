package com.pezesha.actions;

public class WithdrawMoneyAction extends PostAction {

    public WithdrawMoneyAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }

    @Override
    public String getActionString() {
        return "Withdraw Money";
    }

    @Override
    public Integer getActionId() {
        return 6;
    }
}
