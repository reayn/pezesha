package com.pezesha.actions;

public class BorrowMoneyAction extends PostAction {

    public BorrowMoneyAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }

    public String getActionString() {
        return "Borrow Money";
    }

    public Integer getActionId() {
        return 2;
    }
}
