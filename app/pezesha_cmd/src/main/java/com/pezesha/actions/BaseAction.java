package com.pezesha.actions;

import okhttp3.MediaType;

public abstract class BaseAction implements Action {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    protected String baseUrl = "http://127.0.0.1";
    protected Integer port = 8080;
    protected String path;

    public BaseAction(String baseUrl, Integer port) {
        this.baseUrl = baseUrl;
        this.port = port;
    }

    protected void setPath(String path) {
        this.path = path;
    }
}
