package com.pezesha.actions;

import com.pezesha.model.User;

public class AddUserAction extends PostAction {

    private User user;

    public AddUserAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String getActionString() {
        return "Add User";
    }

    @Override
    public Integer getActionId() {
        return 1;
    }
}
