package com.pezesha.actions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

public abstract class PostAction extends BaseAction {

    private Object bodyObj;

    public PostAction(String baseUrl, Integer port) {
        super(baseUrl, port);
    }

    protected void setBody(Object body) {
        this.bodyObj = body;
    }

    public String performAction()
            throws IOException {
        String url = baseUrl + ":" + port + path;
        OkHttpClient client = new OkHttpClient();

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();
        RequestBody body = RequestBody.create(gson.toJson(bodyObj), JSON);
        Request request = new Request.Builder()
                                    .url(url)
                                    .post(body)
                                    .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
