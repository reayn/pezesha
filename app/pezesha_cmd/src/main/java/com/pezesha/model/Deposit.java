package com.pezesha.model;

import java.util.Date;

public class Deposit {

    private Integer depositId;
    private Integer userId;
    private Date depositDate;
    private Double depositAmount;

    public Deposit() {}

    public Deposit(Integer depositId, Integer userId,
                   Date depositDate, Double depositAmount) {
        this.setDepositId(depositId);
        this.setUserId(userId);
        this.setDepositDate(depositDate);
        this.setDepositAmount(depositAmount);
    }

    public Integer getDepositId() {
        return depositId;
    }

    public void setDepositId(Integer depositId) {
        this.depositId = depositId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(Date depositDate) {
        this.depositDate = depositDate;
    }

    public Double getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Double depositAmount) {
        this.depositAmount = depositAmount;
    }
}
