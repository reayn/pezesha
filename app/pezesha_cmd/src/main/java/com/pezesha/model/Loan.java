package com.pezesha.model;

import java.util.Date;

public class Loan {

    public enum Status {
        PAID, PENDING
    }

    private Integer loanId;
    private Integer loanApplicationId;
    private Integer depositId;
    private Date disbursementDate;
    private Status status;

    public Loan() {}

    public Loan(Integer loanId, Integer loanApplicationId,
                Integer depositId, Date disbursementDate,
                Status status) {
        this.setLoanId(loanId);
        this.setLoanApplicationId(loanApplicationId);
        this.setDepositId(depositId);
        this.setDisbursementDate(disbursementDate);
        this.setStatus(status);
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Integer getLoanApplicationId() {
        return loanApplicationId;
    }

    public void setLoanApplicationId(Integer loanApplicationId) {
        this.loanApplicationId = loanApplicationId;
    }

    public Integer getDepositId() {
        return depositId;
    }

    public void setDepositId(Integer depositId) {
        this.depositId = depositId;
    }

    public Date getDisbursementDate() {
        return disbursementDate;
    }

    public void setDisbursementDate(Date disbursementDate) {
        this.disbursementDate = disbursementDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
