package com.pezesha.model;

public class LoanRepayment {

    private Integer loanRepaymentId;
    private Integer loanId;
    private Double amount;
    private String message;
    private Status status;

    public enum Status {
        PAID, FAILED
    }

    public LoanRepayment() {}

    public LoanRepayment(Integer loanRepaymentId, Integer loanId,
                         Double amount, String message,
                         Status status) {
        this.setLoanRepaymentId(loanRepaymentId);
        this.setLoanId(loanId);
        this.setAmount(amount);
        this.setMessage(message);
        this.setStatus(status);
    }

    public Integer getLoanRepaymentId() {
        return loanRepaymentId;
    }

    public void setLoanRepaymentId(Integer loanRepaymentId) {
        this.loanRepaymentId = loanRepaymentId;
    }

    public Integer getLoanId() {
        return loanId;
    }

    public void setLoanId(Integer loanId) {
        this.loanId = loanId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
